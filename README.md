# Desi programmer Codes

### All these Source Codes were hosted on ***Ghostbin*** earlier but  since the service was shut Down and most of you were asking for  these SOURCE CODES. I have ***Re-written*** these source codes.

## **UPDATE**

## **Python GUI And CLI Task Manager**
#### Code In the pytodo Directory

># Tutorial :  [Task Manager](https://www.youtube.com/watch?v=E91x95bSOP4)

### While almost everything is same, still if you see any issue or error feel free to leave that as an issue or contact me directly on...  

##  [Instagram](https://www.instagram.com/desiprogrammer/)
##  [Youtube](https://www.youtube.com/c/desiprogrammer)
##  [Facebook](https://www.facebook.com/desiprogrammerprince)
##  [Github](htttp://www.instagram.com/desiprogrammer)

## Python Codes

### [Rot-13 Encryption-Decryption in Python](https://youtu.be/XxuERMz097M)

### [Morse Code In Python](https://youtu.be/o73bTFX1_Eg)


## C++ Codes

### [Contact management In C++](https://youtu.be/z6t2PpzExu8)


## Tkinter Apps

### [Jumbbled Words In Tkinter](https://youtu.be/noEOUFZySEM)

### [Calculator In tkinter](https://youtu.be/ykF6dx-6kGc)